import logging
import random
import time
import base64
from io import BytesIO

from flask import Flask, jsonify, request
from flask_cors import CORS
import numpy as np
import tensorflow as tf
from PIL import Image
import boto3

BUCKET_NAME = 'tensorflow-model-adidas'
MODEL_FILE_NAME = 'nike_adidas.frozen.pb'

application = Flask(__name__)
CORS(application)
application.config.from_object(__name__)

S3 = boto3.client('s3', region_name='eu-west-1')

def load_model(bucket_name, key):    
    # Load model from S3 bucket
    response = S3.get_object(Bucket=bucket_name, Key=key)
    # Load pickle model
    model_str = response['Body'].read()
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
    # model = pickle.load( open( response['Body'].read()))
    return graph_def



########### To run locally, use this code
# This could be added to the Flask configuration
MODEL_PATH = 'nike_adidas.frozen.pb'

# Read the graph definition file
with open(MODEL_PATH, 'rb') as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())

###### Get model from S3 bucket

#graph_def = load_model(BUCKET_NAME, MODEL_FILE_NAME)

# Load the graph stored in `graph_def` into `graph`
graph = tf.Graph() 
with graph.as_default():
    tf.import_graph_def(graph_def, name='')

def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8) 

# Enforce that no new nodes are added
graph.finalize()

# Create the session that we'll use to execute the model
sess_config = tf.ConfigProto(
    log_device_placement=False,
    allow_soft_placement = True,
    gpu_options = tf.GPUOptions(
        per_process_gpu_memory_fraction=1
    )
)
sess = tf.Session(graph=graph, config=sess_config)

# Use print(sess.graph.get_operations()) to see all operational layers in the model

# Get the input and output operations
input_op = graph.get_operation_by_name('image_tensor')
input_tensor = input_op.outputs[0]

detection_boxes = graph.get_operation_by_name('detection_boxes')
detection_scores = graph.get_operation_by_name('detection_scores')
detection_classes = graph.get_operation_by_name('detection_classes')
num_detections = graph.get_operation_by_name('num_detections')

detection_boxes = detection_boxes.outputs
detection_scores = detection_scores.outputs
detection_classes = detection_classes.outputs
num_detections = num_detections.outputs

threshold = 0.99

# All we need to classify an image is:
# `sess` : we will use this session to run the graph (this is thread safe)
# `input_tensor` : we will assign the image to this placeholder
# `output_tensor` : the predictions will be stored here


@application.route('/', methods=['GET', 'POST'])
def classify():
    application.logger.info("Classifying image")

    # Load in an image to classify and preprocess it
    #image = Image.open(file_path)
    image = Image.open(BytesIO(base64.b64decode(request.data)))
    image = image.convert('RGB')
    print(image.mode)
    image_np = load_image_into_numpy_array(image)
    images = np.expand_dims(image, 0)
    
    # Get the predictions (output of the softmax) for this image
    t = time.time()
    preds = sess.run([detection_boxes, detection_scores, detection_classes, num_detections], {input_tensor : images})

    dt = time.time() - t
    application.logger.info("Execution time: %0.2f" % (dt * 1000.))
    # Single image in this batch
    boxes = preds[0][0][0]
    scores = preds[1][0][0]
    classes = preds[2][0][0]
    print(boxes)
    print(scores)
    predictions = list(filter(lambda x: x > threshold, scores))
    print(predictions)
    # The probabilities should sum to 1
    return jsonify(detection_boxes = boxes[0:len(predictions)].tolist(),
                    classes = classes[0:len(predictions)].tolist(),
                    scores = scores[0:len(predictions)].tolist(),
                    prediction_time=dt*1000)

if __name__ == '__main__':

    application.run(debug=True, port=8009)