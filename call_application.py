import requests
import base64

def post_image(image_path):
    with open(image_path, 'rb') as image_file:
        encoded_image = base64.b64encode(image_file.read())
        URL = 'http://logospotter-tf-env.4n2j2nic3w.eu-west-1.elasticbeanstalk.com/'
        r = requests.post(URL, data= encoded_image)   
        print(r)
        print(r.text)

## Provide path to a valid jpg here
post_image("screenshots/00004715.jpg")