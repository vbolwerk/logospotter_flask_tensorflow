# logospotter_flask_tensorflow

### Prerequisites

In order to run the flask app locally, make sure that you have the following prerequisites installed:

_python_

_python virtualenv (in order to install, enter pip install virtualenv in the command line)_

### Installation (local)

To install, enter the following commands in your command line.

##### To create and activate virtual environment in python:

_virtualenv logospotter_env_

_logospotter_env/Scripts/activate_

_pip install -r requirements.txt_

##### To run flask app locally

_python application.py_

##### To deactivate virtual environment

_deactivate_ in main folder

### Deployment (with AWS Elastic Beanstalk)

These are instuctions for deploying to AWS. In order to do that, you will need AWS credentials. The following resource helps you to set up your AWS credentials:

- https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html

The following resources can be used as references for deployment on AWS Elastic Beanstalk:

- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/eb-cli3-install-windows.html
- https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/create-deploy-python-flask.html

##### Installing AWS Elastic beanstalk cli (in line with official documentation above)

In order to deploy properly, you will need to have the AWS cli installed. The following steps are required:

_pip install awsebcli --upgrade --user_ (this installs the AWS Elastic Beanstalk cli)

Now the tricky part: you will have to add the executable path in windows (which is currently possible for your own account).

- Press the Windows key, and then type environment variables.
- Choose Edit environment variables for your account.
- Choose PATH under user variables, and then choose Edit.
- Click "new" and add the following to your path: %USERPROFILE%\AppData\roaming\Python\Python36\scripts
- Choose ok twice to apply new setting
- Open a new command prompt and enter _eb --version_ in order to verify your installation

##### Deploy Python flask (in line with official documentation above)

This deployment example assumes _eu-west-1_ as deployment region and an AWS profile named _personal_.

1.  Activate your virtual environment and install requirements if you have not already done so (see instructions under local installation above).
2.  _eb init -p python-3.6 logospotter --region eu-west-1 --profile personal_
3.  _eb create logospotter-env_
4.  _eb open_ to go to the endpoint you have just created (in Logo Spotter case this will lead to an internal server error, because the app expects an image)
5.  Change the URL in logospotter_call.py to test your newly created endpoint!
